/**Get marker based on location category*/
export function categoryMarker(category) {
    switch (category) {
        case "Parking":
            return require("../img/category/parking.png");
            break;
        case "Bus Park":
            return require("../img/category/bus-park.png");
            break;
        case "Eating Places":
            return require("../img/category/eating-places.png");
            break;
        case "Printery":
            return require("../img/category/printery.png");
            break;
        case "Library":
            return require("../img/category/library.png");
            break;
        case "Classroom":
            return require("../img/category/classroom.png");
            break;
        case "Office":
            return require("../img/category/office.png");
            break;
        default:
            console.log("Sorry, no image for category " + category + ".");
    }
}

export function renderIf(condition, content) {
    if (condition) {
        return content;
    } else {
        return null;
    }
}
