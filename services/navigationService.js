import haversine from "haversine";
/**Convert lat/long to meters using projection */

export function latLongToMerc(latDeg, lonDeg) {
    let lonRad = lonDeg / 180.0 * Math.PI;
    let latRad = latDeg / 180.0 * Math.PI;
    let sm_a = 6378137.0;
    let xMeters = sm_a * lonRad;
    let yMeters = sm_a * Math.log((Math.sin(latRad) + 1) / Math.cos(latRad));
    return { x: xMeters, y: yMeters };
}

export function transformPointToAR(itemPosition, currentPosition) {
    const objPoint = latLongToMerc(itemPosition.x, itemPosition.y);
    const devicePoint = latLongToMerc(currentPosition.x, currentPosition.y);
    // latitude(north,south) maps to the z axis in AR
    // longitude(east, west) maps to the x axis in AR
    let objFinalPosX = objPoint.x - devicePoint.x;
    let objFinalPosZ = objPoint.y - devicePoint.y;
    //flip the z, as negative z(is in front of us which is north, pos z is behind(south).
    return { x: objFinalPosX, z: -objFinalPosZ };
}

export function filterLocations(locations, key) {
    return locations.filter(location => {
        return location.category === key;
    });
}

export function mapToWorldSpace(locations, currentPosition) {
    return locations.map(location => {
        let itemPosition = { x: location.latitude, y: location.longitude };
        return transformPointToAR(itemPosition, currentPosition);
    });
}

export function getMeterDistance(locations, currentPosition) {
    let position = {
        latitude: currentPosition.x,
        longitude: currentPosition.y
    };
    return locations.map(location => {
        let itemPosition = {
            latitude: location.latitude,
            longitude: location.longitude
        };
        return Math.round(haversine(position, itemPosition, { unit: "meter" }));
    });
}
