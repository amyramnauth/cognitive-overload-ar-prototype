/**
 * Copyright (c) 2017-present, Viro, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

import React, { Component } from "react";
import {
    AppRegistry,
    ActivityIndicator,
    Alert,
    Text,
    View,
    Image,
    StyleSheet,
    PixelRatio,
    TouchableHighlight
} from "react-native";

import { ViroARSceneNavigator } from "react-viro";
import Swiper from "react-native-swiper";
import { renderIf } from "./services/helper";
/*
 TODO: Insert your API key below
 */
var sharedProps = {
    apiKey: "BF7D8BEE-A60F-408D-AAFB-4DF38075A8D4",
    worldAlignment: "GravityAndHeading"
};

// Sets the default scene you want for AR and VR
var InitialChecklistScene = require("./components/ChecklistScene");
var InitialCategoryScene = require("./components/CategoryScene");
var InitialQuickGuideScene = require("./components/QuickGuideScene");

var UNSET = "UNSET";
var AR_NAVIGATOR_TYPE = "AR";
var QUICK_GUIDE_START = "QUICK_GUIDE_START";
var QUICK_GUIDE_APPROACH = "QUICK_GUIDE";
var CHECKLIST_APPROACH = "CHECKLIST";
var CATEGORY_APPROACH = "CATEGORY";

// This determines which type of experience to launch in, or UNSET, if the user should
// be presented with a choice of AR or VR. By default, we offer the user a choice.
var defaultNavigatorType = UNSET;

export default class ViroSample extends Component {
    constructor() {
        super();

        this.state = {
            navigatorType: defaultNavigatorType,
            sharedProps: sharedProps,
            viroAppProps: {
                trackingInitCallBack: this.trackingInitCallBack,
                trackingInitialized: false,
                objCategory: "Classroom"
            }
        };
        this._getExperienceSelector = this._getExperienceSelector.bind(this);
        this._getCategoryNavigator = this._getCategoryNavigator.bind(this);
        this._getQuickGuideSlider = this._getQuickGuideSlider.bind(this);
        this._getExperienceButtonOnPress = this._getExperienceButtonOnPress.bind(
            this
        );
        this._onCategorySelect = this._onCategorySelect.bind(this);
        this.trackingInitCallBack = this.trackingInitCallBack.bind(this);
        this._exitViro = this._exitViro.bind(this);
    }

    // Replace this function with the contents of _getVRNavigator() or _getARNavigator()
    // if you are building a specific type of experience.
    render() {
        switch (this.state.navigatorType) {
            case UNSET:
                return this._getExperienceSelector();
                break;

            case CATEGORY_APPROACH:
                return this._getCategoryNavigator();
                break;

            case QUICK_GUIDE_START:
                return this._getQuickGuideSlider();
                break;

            case QUICK_GUIDE_APPROACH:
                return this._getQuickGuideNavigator();
                break;

            case CHECKLIST_APPROACH:
                return this._getChecklistNavigator();
                break;

            default:
                return this._getExperienceSelector();
                break;
        }
    }

    // Presents the user with a choice of an AR or VR experience
    _getExperienceSelector() {
        return (
            <View style={localStyles.outer}>
                <View style={localStyles.inner}>
                    <Text style={localStyles.titleText}>
                        Choose your desired approach:
                    </Text>
                    <TouchableHighlight
                        style={localStyles.buttons}
                        onPress={this._getExperienceButtonOnPress(
                            QUICK_GUIDE_START
                        )}
                        underlayColor={"#68a0ff"}
                    >
                        <Text style={localStyles.buttonText}>Quick Guide</Text>
                    </TouchableHighlight>

                    <TouchableHighlight
                        style={localStyles.buttons}
                        onPress={this._getExperienceButtonOnPress(
                            CHECKLIST_APPROACH
                        )}
                        underlayColor={"#68a0ff"}
                    >
                        <Text style={localStyles.buttonText}>Checklist</Text>
                    </TouchableHighlight>

                    <TouchableHighlight
                        style={localStyles.buttons}
                        onPress={this._getExperienceButtonOnPress(
                            CATEGORY_APPROACH
                        )}
                        underlayColor={"#68a0ff"}
                    >
                        <Text style={localStyles.buttonText}>Category</Text>
                    </TouchableHighlight>
                </View>
            </View>
        );
    }

    /**Returns the QuickGuideScene which will start the Quick Guide experience */
    _getQuickGuideSlider() {
        return (
            <Swiper showsButtons={true}>
                <View style={localStyles.slide1}>
                    {/* <Image
                        style={localStyles.backgroundImage}
                        source={require("./img/slideshow/arrow.png")}
                    /> */}
                    <Text style={localStyles.text}>Move camera around</Text>
                </View>
                <View style={localStyles.slide2}>
                    {/* <Image
                        style={localStyles.backgroundImage}
                        source={require("./img/slideshow/location.png")}
                    /> */}
                    <Text style={localStyles.text}>Find a location</Text>
                </View>
                <View style={localStyles.slide3}>
                    <Text style={localStyles.text}>
                        Get meter distance from you
                    </Text>
                </View>
                <View style={localStyles.slide2}>
                    {/* <Image
                        style={localStyles.backgroundImage}
                        source={require("./img/slideshow/more.png")}
                    /> */}
                    <Text style={localStyles.text}>
                        Click location for more info
                    </Text>
                </View>
                <View style={localStyles.slide3}>
                    <Text style={localStyles.text}>Begin experience</Text>
                    <View
                        style={{
                            position: "absolute",
                            left: 0,
                            right: 0,
                            bottom: 77,
                            alignItems: "center"
                        }}
                    >
                        <TouchableHighlight
                            style={localStyles.startButton}
                            onPress={this._getExperienceButtonOnPress(
                                QUICK_GUIDE_APPROACH
                            )}
                            underlayColor={"#92BBD9"}
                        >
                            <Image source={require("./img/start.png")} />
                        </TouchableHighlight>
                    </View>
                </View>
            </Swiper>
        );
    }

    /**Returns the Cateogry Navigator which will start the Category-based experience */
    _getCategoryNavigator() {
        return (
            <View style={localStyles.arOuter}>
                <ViroARSceneNavigator
                    {...this.state.sharedProps}
                    initialScene={{ scene: InitialCategoryScene }}
                    viroAppProps={this.state.viroAppProps}
                    onExitViro={this._exitViro}
                />
                {renderIf(
                    !this.state.viroAppProps.trackingInitialized,
                    <View
                        style={{
                            position: "absolute",
                            left: 0,
                            right: 0,
                            top: 0,
                            bottom: 0,
                            alignItems: "center",
                            justifyContent: "center"
                        }}
                    >
                        <ActivityIndicator
                            size="large"
                            animating={
                                !this.state.viroAppProps.trackingInitialized
                            }
                            color="#ffffff"
                        />
                    </View>
                )}
            </View>
        );
    }

    _getQuickGuideNavigator() {
        return (
            <View style={localStyles.arOuter}>
                <ViroARSceneNavigator
                    {...this.state.sharedProps}
                    initialScene={{ scene: InitialQuickGuideScene }}
                    onExitViro={this._exitViro}
                    viroAppProps={this.state.viroAppProps}
                />
                {renderIf(
                    !this.state.viroAppProps.trackingInitialized,
                    <View
                        style={{
                            position: "absolute",
                            left: 0,
                            right: 0,
                            top: 0,
                            bottom: 0,
                            alignItems: "center",
                            justifyContent: "center"
                        }}
                    >
                        <ActivityIndicator
                            size="large"
                            animating={
                                !this.state.viroAppProps.trackingInitialized
                            }
                            color="#ffffff"
                        />
                    </View>
                )}
            </View>
        );
    }

    _getChecklistNavigator() {
        return (
            <View style={localStyles.arOuter}>
                <ViroARSceneNavigator
                    {...this.state.sharedProps}
                    style={localStyles.arView}
                    initialScene={{ scene: InitialChecklistScene }}
                    viroAppProps={this.state.viroAppProps}
                    onExitViro={this._exitViro}
                />
                {/* {renderIf(
                    !this.state.viroAppProps.trackingInitialized,
                    <View
                        style={{
                            position: "absolute",
                            left: 0,
                            right: 0,
                            top: 0,
                            bottom: 0,
                            alignItems: "center",
                            justifyContent: "center"
                        }}
                    >
                        <ActivityIndicator
                            size="large"
                            animating={
                                !this.state.viroAppProps.trackingInitialized
                            }
                            color="#ffffff"
                        />
                    </View>
                )} */}
                <View
                    style={{
                        position: "absolute",
                        left: 0,
                        right: 0,
                        bottom: 77,
                        alignItems: "center"
                    }}
                >
                    <TouchableHighlight
                        style={localStyles.filterButtons}
                        onPress={this._onCategorySelect}
                        underlayColor={"#00000000"}
                    >
                        <Image source={require("./img/btn_mode_objects.png")} />
                    </TouchableHighlight>
                </View>
            </View>
        );
    }
    // This function returns an anonymous/lambda function to be used
    // by the experience selector buttons
    _getExperienceButtonOnPress(navigatorType) {
        return () => {
            this.setState({
                navigatorType: navigatorType
            });
        };
    }

    _onCategorySelect() {
        Alert.alert("Choose a category", "Select where you want to go!", [
            {
                text: "Classroom",
                onPress: () =>
                    this.setState({
                        viroAppProps: {
                            objCategory: "Classroom"
                        }
                    })
            },
            {
                text: "Eating Places",
                onPress: () =>
                    this.setState({
                        viroAppProps: {
                            objCategory: "Eating Places"
                        }
                    })
            },
            {
                text: "Library",
                onPress: () =>
                    this.setState({
                        viroAppProps: {
                            objCategory: "Library"
                        }
                    })
            },
            {
                text: "Office",
                onPress: () =>
                    this.setState({
                        viroAppProps: {
                            objCategory: "Office"
                        }
                    })
            },
            {
                text: "Parking",
                onPress: () =>
                    this.setState({
                        viroAppProps: {
                            objCategory: "Parking"
                        }
                    })
            },
            {
                text: "Printery",
                onPress: () =>
                    this.setState({
                        viroAppProps: {
                            objCategory: "Printery"
                        }
                    })
            }
        ]);
    }

    /**Callback function to get child trackingInitialized */
    trackingInitCallBack = () => {
        this.setState({
            viroAppProps: {
                trackingInitialized: true
            }
        });
    };

    // This function "exits" Viro by setting the navigatorType to UNSET.
    _exitViro() {
        this.setState({ navigatorType: UNSET });
    }
}

const localStyles = StyleSheet.create({
    viroContainer: {
        flex: 1,
        backgroundColor: "black"
    },
    outer: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: "black"
    },
    inner: {
        flex: 1,
        flexDirection: "column",
        alignItems: "center",
        backgroundColor: "black"
    },
    arView: {
        flex: 1
    },
    arOuter: {
        flex: 1
    },
    titleText: {
        paddingTop: 30,
        paddingBottom: 20,
        color: "#fff",
        textAlign: "center",
        fontSize: 25
    },
    buttonText: {
        color: "#fff",
        textAlign: "center",
        fontSize: 20
    },
    buttons: {
        height: 80,
        width: 150,
        paddingTop: 20,
        paddingBottom: 20,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: "#68a0cf",
        borderRadius: 10,
        borderWidth: 1,
        borderColor: "#fff"
    },
    startButton: {
        height: 80,
        width: 80,
        paddingTop: 20,
        paddingBottom: 20,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: "#00000000",
        borderRadius: 10,
        borderWidth: 1,
        borderColor: "#ffffff00"
    },
    filterButtons: {
        height: 80,
        width: 80,
        paddingTop: 20,
        paddingBottom: 20,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: "#00000000",
        borderRadius: 10,
        borderWidth: 1,
        borderColor: "#ffffff00"
    },
    exitButton: {
        height: 50,
        width: 100,
        paddingTop: 10,
        paddingBottom: 10,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: "#68a0cf",
        borderRadius: 10,
        borderWidth: 1,
        borderColor: "#fff"
    },
    slide1: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#9DD6EB"
    },
    slide2: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#97CAE5"
    },
    slide3: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#92BBD9"
    },
    text: {
        color: "#fff",
        fontSize: 25,
        fontWeight: "bold"
    },
    categoryTextStyles: {
        fontFamily: "Arial",
        fontSize: 30,
        color: "#ffffff",
        textAlignVertical: "center",
        textAlign: "center"
    }
});

module.exports = ViroSample;
