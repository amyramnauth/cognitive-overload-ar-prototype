"use strict";

import React, { Component } from "react";

import { ActivityIndicator, Alert, View, StyleSheet } from "react-native";
import {
    ViroARScene,
    ViroText,
    ViroConstants,
    ViroImage,
    ViroNode
} from "react-viro";
import * as navigationService from "../services/navigationService";
import { categoryMarker } from "../services/helper";
import data from "../resources/data2.json";

export default class CategoryScene extends Component {
    constructor() {
        super();

        // Set initial state here
        this.state = {
            locationCoordinates: [],
            locationDistances: []
        };

        // bind 'this' to functions
        this._onDisplayDialog = this._onDisplayDialog.bind(this);
        this._onTrackingUpdated = this._onTrackingUpdated.bind(this);
    }

    render() {
        return (
            <ViroARScene
                ref="arscene"
                onTrackingUpdated={this._onTrackingUpdated}
            >
                {this.state.locationCoordinates.map((location, index) => (
                    <ViroNode key={index}>
                        <ViroText
                            key={location.id}
                            text={data[index].name + " [" + this.state.locationDistances[index] + " m]"} //prettier-ignore
                            scale={[3, 3, 3]}
                            transformBehaviors={["billboard"]}
                            position={[location.x, 0, location.z]}
                            style={styles.helloWorldTextStyle}
                            onClick={this._onDisplayDialog(data[index])}
                        />

                        <ViroImage
                            key={location.id}
                            height={2}
                            width={2}
                            scale={[1, 1, 1]}
                            position={[location.x - 1, 0, location.z - 2]}
                            placeholderSource={require("../img/spinner.png")}
                            source={categoryMarker(data[index].category)}
                        />
                    </ViroNode>
                ))}
            </ViroARScene>
        );
    }

    _onTrackingUpdated(state, reason) {
        if (state == ViroConstants.TRACKING_NORMAL) {
            this.props.arSceneNavigator.viroAppProps.trackingInitCallBack();

            navigator.geolocation.getCurrentPosition(position => {
                let currentPosition = {
                    x: position.coords.latitude,
                    y: position.coords.longitude
                };
                let roomPointToAR = navigationService.mapToWorldSpace(
                    data,
                    currentPosition
                );
                // console.log("room point to AR", roomPointToAR);
                let meterDistances = navigationService.getMeterDistance(
                    data,
                    currentPosition
                );
                this.setState({
                    text: "Hello World!",
                    pointX: roomPointToAR[0].x,
                    pointZ: roomPointToAR[0].z,
                    locationCoordinates: roomPointToAR,
                    locationDistances: meterDistances
                });
            }),
                err => {
                    console.warn(`ERROR(${err.code}): ${err.message}`);
                },
                {
                    /**optionsBody */
                    maximumAge: 60000,
                    timeout: 5000,
                    enableHighAccuracy: true
                };

            /**watch position and re-render every three seconds if necessary */
            setInterval(() => {
                navigator.geolocation.watchPosition(
                    position => {
                        let newPosition = {
                            x: position.coords.latitude,
                            y: position.coords.longitude
                        };
                        /**only if position changes, then do a re-render */
                        let updatedPointToAR = navigationService.mapToWorldSpace(
                            data,
                            newPosition
                        );
                        let updatedDistances = navigationService.getMeterDistance(
                            data,
                            newPosition
                        );
                        // console.log("room point to AR", updatedPointToAR);
                        this.setState({
                            locationCoordinates: updatedPointToAR,
                            locationDistances: updatedDistances
                        });
                    },
                    err => {
                        console.warn(`ERROR(${err.code}): ${err.message}`);
                    }
                );
            }, 3000);
        } else if (state == ViroConstants.TRACKING_NONE) {
            alert("Please move phone around. No tracking detected");
        }
    }

    /**Show more details on location */
    _onDisplayDialog(object) {
        return source =>
            Alert.alert(
                `${object.category} - ${object.name}`,
                `${object.info}`,
                [
                    {
                        text: "OK",
                        style: "cancel"
                    }
                ],
                { cancelable: true }
            );
    }
}

var styles = StyleSheet.create({
    helloWorldTextStyle: {
        fontFamily: "Arial",
        fontSize: 20,
        color: "#ffffff",
        textAlignVertical: "center",
        textAlign: "center"
    }
});

module.exports = CategoryScene;
