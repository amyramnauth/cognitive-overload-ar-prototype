"use strict";

import React, { Component } from "react";

import { ActivityIndicator, Alert, View, StyleSheet } from "react-native";
import { ViroARScene, ViroText, ViroConstants } from "react-viro";
import * as navigationService from "../services/navigationService";
import data from "../resources/data2.json";

export default class ChecklistScene extends Component {
    constructor() {
        super();

        // Set initial state here
        this.state = {
            locationCoordinates: [],
            locationDistances: [],
            locationCategories: [],
            currentPos: []
        };

        // bind 'this' to functions
        this._onDisplayDialog = this._onDisplayDialog.bind(this);
        this._onTrackingUpdated = this._onTrackingUpdated.bind(this);
    }

    render() {
        return (
            <ViroARScene
                ref="arscene"
                onTrackingUpdated={this._onTrackingUpdated}
            >
                {/* <ViroText
                    text={
                        this.props.arSceneNavigator.viroAppProps.objCategory +
                        " [" +
                        this.state.distance +
                        " m]"
                    }
                    scale={[0.5, 0.5, 0.5]}
                    position={[0, 0, -1]}
                    style={styles.helloWorldTextStyle}
                /> */}
                {this.state.locationCoordinates.map((location, index) => (
                    <ViroText
                        key={index}
                        text={
                            this.state.locationCategories[index].name +
                            " [" +
                            this.state.locationDistances[index] +
                            " m]"
                        }
                        scale={[3, 3, 3]}
                        transformBehaviors={["billboard"]}
                        position={[location.x, 0, location.z]}
                        style={styles.helloWorldTextStyle}
                        onClick={this._onDisplayDialog(
                            this.state.locationCategories[index]
                        )}
                    />
                ))}
            </ViroARScene>
        );
    }

    // _onLoadStart() {
    //     this.setState({ isLoading: true });
    // }

    _onTrackingUpdated(state, reason) {
        if (state == ViroConstants.TRACKING_NORMAL) {
            navigator.geolocation.getCurrentPosition(position => {
                let currentPosition = {
                    x: position.coords.latitude,
                    y: position.coords.longitude
                };
                let categoryPoints = navigationService.filterLocations(
                    data,
                    this.props.arSceneNavigator.viroAppProps.objCategory
                );
                let roomPointToAR = navigationService.mapToWorldSpace(
                    categoryPoints,
                    currentPosition
                );
                let meterDistances = navigationService.getMeterDistance(
                    categoryPoints,
                    currentPosition
                );
                this.setState({
                    locationCoordinates: roomPointToAR,
                    locationCategories: categoryPoints,
                    locationDistances: meterDistances,
                    currentPos: currentPosition
                });
            }),
                err => {
                    console.warn(`ERROR(${err.code}): ${err.message}`);
                },
                {
                    /**optionsBody */
                    maximumAge: 60000,
                    timeout: 5000,
                    enableHighAccuracy: true
                };

            /**watch position and re-render every three seconds if necessary */
            setInterval(() => {
                navigator.geolocation.watchPosition(
                    position => {
                        let newPosition = {
                            x: position.coords.latitude,
                            y: position.coords.longitude
                        };

                        this.setState({
                            currentPos: newPosition
                        });
                    },
                    err => {
                        console.warn(`ERROR(${err.code}): ${err.message}`);
                    }
                );

                /**only if position changes, then do a re-render */
                let updatedCategoryPoints = navigationService.filterLocations(
                    data,
                    this.props.arSceneNavigator.viroAppProps.objCategory
                );
                let updatedPointToAR = navigationService.mapToWorldSpace(
                    updatedCategoryPoints,
                    this.state.currentPos
                );
                let updatedDistances = navigationService.getMeterDistance(
                    updatedCategoryPoints,
                    this.state.currentPos
                );
                this.setState({
                    locationCoordinates: updatedPointToAR,
                    locationCategories: updatedCategoryPoints,
                    locationDistances: updatedDistances
                });
            }, 3000);
        } else if (state == ViroConstants.TRACKING_NONE) {
            alert("Please move phone around. No tracking detected");
        }
    }

    /**Show more details on location */
    _onDisplayDialog(object) {
        return source =>
            Alert.alert(
                `${object.category} - ${object.name}`,
                `${object.info}`,
                [
                    {
                        text: "OK",
                        style: "cancel"
                    }
                ],
                { cancelable: true }
            );
    }
}

var styles = StyleSheet.create({
    helloWorldTextStyle: {
        fontFamily: "Arial",
        fontSize: 20,
        color: "#ffffff",
        textAlignVertical: "center",
        textAlign: "center"
    }
});

module.exports = ChecklistScene;
