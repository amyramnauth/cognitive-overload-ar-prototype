<br/>
<br/>
<br/>
<p align="center">
    <img alt="awesome" src="https://static1.squarespace.com/static/5627eb27e4b00e3c672920f6/t/5a53cb1d8165f595006bc56b/1519167518691/viroreact_logo_color_w_c.png" width="480" />
</p>

<br/>
<br/>
<br/>
<br/>
<br/>
<p align="center">
<b>Built with React Native and ViroReact</b>
</p>
<br/>
<br/>

## Compatible Devices

<div>
  <h3>
    <p align="left">AR is limited to ARKit and ARCore supported devices</p>
  </h3>
  <p>ARKit supported devices <a href="https://developer.apple.com/library/content/documentation/DeviceInformation/Reference/iOSDeviceCompatibility/DeviceCompatibilityMatrix/DeviceCompatibilityMatrix.html">here</a> (Look for <code>arkit</code>)</p>
<p>ARCore supported devices <a href="https://developers.google.com/ar/discover/#supported_devices">here</a></p>
</div>

## Installation

```bash
npm install
```

## Usage

<ol>
<li value="1">Install the Viro Media App from the <a href="https://itunes.apple.com/us/app/viro-media/id1163100576?mt=8">AppStore</a> on your iOS device, or from the <a href="https://play.google.com/store/apps/details?id=com.viromedia.viromedia">Play Store</a> for your Android device.</li>
<li value="2">From the root of your project, run the command: <code>npm start</code> to start the packager server.</li>
<li value="3">Launch the Viro Media App, open the left panel and tap "Enter Testbed"</li>
<li value="4">Find your local IP, enter it into the text field on the Testbed screen, then press "Go"</li>
</ol>

## Note

<p align="center">
Only tested with iOS
</p>
